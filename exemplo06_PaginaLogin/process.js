const{createApp} = Vue;

createApp ({
    data (){
        return{
            usuario: '',
            senha:'',
            erro:null,
            sucesso:null,
        }//fechamento return
    },//fechamento data
    
    methods:{
    Login(){
            // alert("Testando...");

            //simulando uma requisição de login assincrona
            setTimeout(() => {
                if((this.usuario === "miguel" && this.senha === "12345678" )
                || (this.usuario === "julia" && this.senha === "123456")){
                    this.erro= null;
                    this.sucesso= "Login efetuado com sucesso!";
                    // alert("Login efetuado com sucesso!");
                }//Fim do if
                else{
                    // alert("Usuario ou Senha Incorretos");
                    this.erro = "Usuario ou senha incorretos!";
                    this.sucesso = null;
                }
             }, 1000);

         }, //fechamento Login
    },//fechamento methods

}).mount("#app");//fechamento app