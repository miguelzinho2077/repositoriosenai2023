const { createApp } = Vue;
createApp({
  data() {
    return {
      valorDisplay: "0",
      operador: null,
      numeroAtual: null,
      numeroAnterior: null,
      tamanholetra: 50 + "px",

      usuario: "",
      senha: "",
      erro: null,
      sucesso: null,
    }; //fechamento return
  }, //fechamento data

  methods: {
    getNumero(numero) {
      if (this.valorDisplay == "0") {
        this.valorDisplay = numero.toString();
      } else {
        if (this.operador == "=") {
          this.valorDisplay = "";
          this.operador = null;
        }
        // this.valorDisplay = this.valorDisplay + numero.toString();

        //adição simplificada
        this.valorDisplay += numero.toString();
      }
    }, //fechamento getNumero

    limpar() {
      this.valorDisplay = "0";
      this.operador = null;
      this.numeroAnterior = null;
      this.numeroAtual = null;
    }, //Fechamento methods

    decimal() {
      if (!this.valorDisplay.includes(".")) {
        this.valorDisplay += ".";
      }
    },
    operacoes(operacao) {
      if (this.numeroAtual != null) {
        const displayAtual = parseFloat(this.valorDisplay);
        if (this.operador != null) {
          switch (this.operador) {
            case "+":
              this.valorDisplay = (this.numeroAtual + displayAtual).toString();
              break;
              break;

            case "-":
              this.valorDisplay = (this.numeroAtual - displayAtual).toString();
              break;

            case "*":
              this.valorDisplay = (this.numeroAtual * displayAtual).toString();
              break;

            case "/":
              if (displayAtual == 0 || this.numeroAtual == 0) {
                this.valorDisplay = "Operação impossivel";
              } else {
                this.valorDisplay = (
                  this.numeroAtual / displayAtual
                ).toString();
              }
              break;
          } //fim do switch
          this.numeroAnterior = this.numeroAtual;
          this.numeroAtual;
          this.numeroAtual = null;
          if (this.operador != "=") {
            this.operador = null;
          }

          //acertar o numero de casas decimais quando o resultado for decimal
          if (this.valorDisplay.includes(".")) {
            const numDecimal = parseFloat(this.valorDisplay);
            this.valorDisplay = numDecimal.toFixed(2).toString();
          }
        } //Fechando if operador
        else {
          this.numeroAnterior = displayAtual;
        } //fim do else
      } //fim do if numeroAtual

      this.operador = operacao;
      this.numeroAtual = parseFloat(this.valorDisplay);
      // this.valorDisplay = "0" ;
      if (this.operador != "=") {
        this.valorDisplay = "0";
      }
    }, //fim das operaçoes

    Login(){
        // alert("Testando...");

        //simulando uma requisição de login assincrona
        setTimeout(() => {
            if((this.usuario === "miguel" && this.senha === "12345678" )
            || (this.usuario === "julia" && this.senha === "123456")){
                this.erro= null;
                this.sucesso= "Login efetuado com sucesso!";
                window.location.href='calculadora.html';
                // alert("Login efetuado com sucesso!");
            }//Fim do if
            else{
                // alert("Usuario ou Senha Incorretos");
                this.erro = "Usuario ou senha incorretos!";
                this.sucesso = null;
                
            }
         }, 1000);

     }, //fechamento Login
  }, //fechamento methods
}).mount("#app"); //fechamento app
