const {createApp} = Vue;
createApp({
    data(){
            return{
                valorDisplay:"0",
                operador:null,
                numeroAtual:null,
                numeroAnterior:null,
                tamanholetra: 50 + "px",
            };//fechamento return
    },//fechamento data

    methods:{
        getNumero(numero){                

                if(this.valorDisplay == "0"){
                    this.valorDisplay = numero.toString();
                }
                else{
                    if(this.operador == "=") {
                        this.valorDisplay = "";
                        this.operador = null;
                    }
                    // this.valorDisplay = this.valorDisplay + numero.toString();

                    //adição simplificada
                    this.valorDisplay += numero.toString ();
                }
        },//fechamento getNumero

        limpar(){
            this.valorDisplay= "0";
            this.operador = null;
            this.numeroAnterior= null;
            this.numeroAtual= null;
        },//Fechamento methods
        
            
        decimal(){
            if(!this.valorDisplay.includes(".")){

                this.valorDisplay += "."
            }
        },
        operacoes (operacao){
             if(this.numeroAtual != null){
                const displayAtual = parseFloat(this.valorDisplay);
                if(this.operador != null){
                    switch(this.operador){
                        case "+":
                            this.valorDisplay =(this.numeroAtual + displayAtual).toString();break;
                            break;

                        case "-":
                            this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                            break;

                        case "*":
                            this.valorDisplay = (this.numeroAtual * displayAtual).toString();
                            break;

                        case "/":
                            if(displayAtual == 0 || this.numeroAtual == 0){ 
                                this.valorDisplay = "Operação impossivel";
                            }
                            else{
                                this.valorDisplay = (this.numeroAtual / displayAtual).toString ();
                            }
                            break;
                    }//fim do switch
                        this.numeroAnterior = this.numeroAtual;
                        this.numeroAtual;
                        this.numeroAtual = null;
                      if(this.operador != "="){
                        this.operador = null;
                      }

                      //acertar o numero de casas decimais quando o resultado for decimal
                      if (this.valorDisplay.includes (".")){
                        const numDecimal = parseFloat(this.valorDisplay);
                        this.valorDisplay = (numDecimal.toFixed(2)).toString();
                      }

                }//Fechando if operador

                else{
                    this.numeroAnterior = displayAtual;
                } //fim do else
            }//fim do if numeroAtual


            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
           // this.valorDisplay = "0" ;
           if(this.operador != "="){
            this.valorDisplay = "0";
           }

        } //fim das operaçoes
    },//fechamento methods
        
}).mount("#app");//fechamento app