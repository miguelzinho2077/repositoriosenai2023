const { createApp } = Vue;
createApp({
  data() {
    return {
      valorDisplay: "0",
      operador: null,
      numeroAtual: null,
      numeroAnterior: null,
      tamanholetra: 50 + "px",

      usuario: "",
      senha: "",
      erro: null,
      sucesso: null,

      //Arrays (vetores) para armazenamento dos nomes de usuarios e senhas
      usuarios: ["admin", "adriano", "julia"],
      senhas: ["1234", "1234", "1234"],
      userAdmin: false,
      mostrarEntrada: false,

      //variaveis para tratamento das informações dos novos usuarios
      newUsername: "",
      newPassword: "",
      confirmPassword: "",

      mostrarLista: false,
    }; //fechamento return
  }, //fechamento data

  methods: {
    getNumero(numero) {
      if (this.valorDisplay == "0") {
        this.valorDisplay = numero.toString();
      } else {
        if (this.operador == "=") {
          this.valorDisplay = "";
          this.operador = null;
        }
        // this.valorDisplay = this.valorDisplay + numero.toString();

        //adição simplificada
        this.valorDisplay += numero.toString();
      }
    }, //fechamento getNumero

    limpar() {
      this.valorDisplay = "0";
      this.operador = null;
      this.numeroAnterior = null;
      this.numeroAtual = null;
    }, //Fechamento methods

    decimal() {
      if (!this.valorDisplay.includes(".")) {
        this.valorDisplay += ".";
      }
    },
    operacoes(operacao) {
      if (this.numeroAtual != null) {
        const displayAtual = parseFloat(this.valorDisplay);
        if (this.operador != null) {
          switch (this.operador) {
            case "+":
              this.valorDisplay = (this.numeroAtual + displayAtual).toString();
              break;
              break;

            case "-":
              this.valorDisplay = (this.numeroAtual - displayAtual).toString();
              break;

            case "*":
              this.valorDisplay = (this.numeroAtual * displayAtual).toString();
              break;

            case "/":
              if (displayAtual == 0 || this.numeroAtual == 0) {
                this.valorDisplay = "Operação impossivel";
              } else {
                this.valorDisplay = (
                  this.numeroAtual / displayAtual
                ).toString();
              }
              break;
          } //fim do switch
          this.numeroAnterior = this.numeroAtual;
          this.numeroAtual;
          this.numeroAtual = null;
          if (this.operador != "=") {
            this.operador = null;
          }

          //acertar o numero de casas decimais quando o resultado for decimal
          if (this.valorDisplay.includes(".")) {
            const numDecimal = parseFloat(this.valorDisplay);
            this.valorDisplay = numDecimal.toFixed(2).toString();
          }
        } //Fechando if operador
        else {
          this.numeroAnterior = displayAtual;
        } //fim do else
      } //fim do if numeroAtual

      this.operador = operacao;
      this.numeroAtual = parseFloat(this.valorDisplay);
      // this.valorDisplay = "0" ;
      if (this.operador != "=") {
        this.valorDisplay = "0";
      }
    }, //fim das operaçoes

    Login() {
      // alert("Testando...");

      //simulando uma requisição de login assincrona
      setTimeout(() => {
        if (
          (this.usuario === "miguel" && this.senha === "12345678") ||
          (this.usuario === "julia" && this.senha === "123456")
        ) {
          this.erro = null;
          this.sucesso = "Login efetuado com sucesso!";
          window.location.href = "calculadora.html";
          // alert("Login efetuado com sucesso!");
        } //Fim do if
        else {
          // alert("Usuario ou Senha Incorretos");
          this.erro = "Usuario ou senha incorretos!";
          this.sucesso = null;
        }
      }, 1000);
    }, //fechamento Login

    login2() {
      this.mostrarEntrada = false;

      setTimeout(() => {
        this.mostrarEntrada = true;
        //verificação de usuario cadastrados ou não nos arrays
        const index = this.usuarios.indexOf(this.usuario);
        if (index !== -1 && this.senhas[index] === this.senha) {
          this.erro = null;
          this.sucesso = "login efetuado com sucesso!";

          //registrando o usuario no localStorage para lembrete de acessos

          localStorage.setItem("usuario", this.usuario);
          localStorage.setItem("senha", this.senha);
          //verificando se o usuario e admin
          if (this.usuario === "admin" && index === 0) {
            this.userAdmin = true;
            this.sucesso = "logado como ADMIN!";
            if (this.usuario === "" && index === 0) {
              this.userAdmin = true;
              this.sucesso = "logado!";
          }
        } //fechamento if
        else {
          this.sucesso = null;
          this.erro = "usuario e / ou senhas incorretas!";
        }
      }, 1000);
    }, //Fechamento login2

    paginaCadastro() {
      this.mostrarEntrada = false;
      if (this.userAdmin == true) {
        this.erro = null;
        this.sucesso = "Carregando Pagina de Cadastro";
        this.mostrarEntrada = true;

        setTimeout(() => {}, 2000);

        setTimeout(() => {
          window.location.href = "cadastro.html";
        }, 1000);
      } else {
        this.sucesso = null;
        setTimeout(() => {
          this.mostrarEntrada = true;
          this.erro = "SEM PRIVILEGIOS ADMIN!!!";
        }, 1000);
      }
    },

    adicionarUsuario() {
      this.mostrarEntrada = false;

      this.usuario = localStorage.getItem("usuario");
      this.senha = localStorage.getItem("senha");
      setTimeout(() => {
        this.mostrarEntrada = true;
        if (this.usuario === "admin") {
          if (
            !this.usuarios.includes(this.newUsername) &&
            this.newUsername != "" &&
            !this.newUsername.includes(" ")
          ) {
            //VValidção da senha digitada para cadastro no array
            if (
              this.newPassword !== "" &&
              !this.newPassword.includes(" ") &&
              this.newPassword === this.confirmPassword
            ) {
              //inserindo  o novo usuario e senha nos arrays
              this.usuarios.push(this.newUsername);
              this.senhas.push(this.newPassword);

              //atualizando o usuario recem cadastrado no localStorage
              localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
              localStorage.setItem("senhas", JSON.stringify(this.senhas));
              
              this.newUsername = "";
              this.newPassword = "";
              this.confirmPassword = "";
              this.erro = null;
              this.sucesso = "Usuario cadastrado com sucesso!";
            } //fechamento if password
            else {
              this.sucesso = null;
              this.erro = "Por favor informe uma senha valida!!!";
              this.newUsername = "";
              this.newPassword = "";
              this.confirmPassword = "";
            } //fechamento else
          } //fechamento if includes
          else {
            this.erro =
              "Usuario já cadastrado ou invalido!Por favor digite um usuário diferente!";
            this.sucesso = null;
            this.newUsername = "";
            this.newPassword = "";
            this.confirmPassword = "";
          } //fechamento else
        } //fechamento if admin
        else {
          this.erro = "não esta logado como ADMIN!";
          this.sucesso = null;
          this.newUsername = "";
          this.newPassword = "";
          this.confirmPassword = "";
        }
      }, 1000);
    },

    listarUsuarios() {
      if (localStorage.getItem("usuarios") && localStorage.getItem("senhas")) {
        this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
        this.senhas = JSON.parse(localStorage.getItem("senhas"));  
      } //Fechamento if
      this.mostrarLista = !this.mostrarLista;
    }, //Fechamento listarUsuarios

    excluirUsuario(usuario) {
      this.mostrarEntrada = false;
      if (usuario === "admin") {
        setTimeout(() => {
          this.mostrarEntrada = true;
          this.sucesso = null;
          this.erro = "o usuario ADMIN não pode ser excluido!!!";
        }, 500);
        return; // Força a saida deste bloco
      } //fechamento do if

      if (confirm("tem certeza que deseja excluir o usuario?")) {
        const index = this.usuarios.indexOf(usuario);
        if (index !== -1) {
          this.usuarios.splice(index, 1);
          this.senhas.splice(index, 1);

          //Atualiza o array usuarios no Localstorage
          localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
          localStorage.setItem("senhas", JSON.stringify(this.senhas));

          setTimeout(() => {
            this.mostrarEntrada = true;
            this.erro = null;
            this.sucesso = "Usuario excluido com sucesso!";
          }, 500);
        } //fechamento if index
      } //fechamento if
    }, //fechamento excluirUsuario
  }, //fechamento methods
}).mount("#app"); //fechamento app
